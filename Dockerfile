FROM cern/cc7-base:latest

RUN yum install -y \
  make \
  gcc-c++ \
  boost-system \
  boost-filesystem \
  boost-devel \
  xsd \
  xerces-c-devel \
  which

WORKDIR /build
COPY . /build

RUN make clean
RUN make dist-clean
RUN make genxsd
RUN make all

ENV LD_LIBRARY_PATH /build/tmUtil:/build/tmXsd:/build/tmTable:/build/tmGrammar:/build/tmEventSetup
ENV UTM_ROOT /build
ENV UTM_XSD_DIR /build/tmXsd

RUN make test

CMD ["/bin/bash"]
